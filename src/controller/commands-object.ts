import { argv } from 'process';
import log from '@ajar/marker';
import { generateId } from '../utils/utils-string.js';
import { ITodo, ITodosList } from '../types.js';
import { save, load, isFileExists } from '../database/data-access.js';

const ALL = "all";
const COMPLETED = "completed";
const OPEN = "open";

interface ICommand{
    function: Function;
    description: string;
}

export interface ICommandsObject {
    [key:string]: ICommand;
}

//class with the available functions as keys
export default class CommandsObject {
    private static instance: CommandsObject;
    [key:string] :ICommand;
    // private _read:ICommand;
    // private _update:ICommand;
    // private _delete:ICommand;
    // private _removeCompleted:ICommand;

    //Using singelton
    private constructor(){

        //create new task
        this.create = {
            function: async function(...title: string[]): Promise<void> {
                const dataList: ITodosList = await load();
                // console.log(dataList);
                let newTask: ITodo = {
                    id: generateId(),
                    title: title.join(' '),
                    isCompleted: false
                }
                dataList.push(newTask);
                log.yellow(`Todo '${newTask.title}' was added to your todos list.\n`)
                save(dataList);
            },
            description: "Create new task(s) by enter the name(s).\n"
        };

        //read tasks by the filter-word  
        this.read = {
            function: async function(filterWord: string): Promise<void> {
                const dataList: ITodosList = await load();
                let listToShow = [];
                if(dataList.length === 0) {
                    log.cyan("Your Todos List is empty.\n")
                }else{
                    log.cyan("Your Todos List:\n");
                    switch(filterWord) {
                        case ALL:
                            console.table(dataList);
                            console.log('\n');
                            return;
                        
                        case COMPLETED:
                            listToShow = dataList.filter(todo=> todo.isCompleted === true);
                            console.table(listToShow);
                            return;
                        
                        case OPEN:
                            listToShow = dataList.filter(todo=> todo.isCompleted === false);
                            console.table(listToShow);
                            return;
        
                        default:
                            console.table(dataList);
                    }
                    
                    // for(const todo of dataList){
                    //     log.blue(todo.title);
                    //     console.table(todo);
                    
                }
                
            },
            description: "  Read tasks: write 'all' / 'completed' / 'open' after the read command\n         in order to display the wanted todos.\n"
        };

        //Mark task as completed
        this.check = {
            function: async function(...tasksIDToBeCompleted: string[]): Promise<void>{
                const dataList: ITodosList = await load();
                for(const taskIDToComplete of tasksIDToBeCompleted){
                    dataList.forEach(todo => {
                        if(todo.id === taskIDToComplete){
                            todo.isCompleted = true;
                            log.yellow(`Todo '${todo.title}' was checked as completed.`);
                        }
                    });
                }
                save(dataList);
            },
            description: "Mark task as completed.\n"
        };

        //Mark task as uncompleted
        this.uncheck = {
            function: async function(...tasksIDToBeUncompleted: string[]): Promise<void>{
                const dataList: ITodosList = await load();
                for(const taskIDTouncomplete of tasksIDToBeUncompleted){
                    dataList.forEach(todo => {
                        if(todo.id === taskIDTouncomplete){
                            todo.isCompleted = false;
                            log.yellow(`Todo '${todo.title}' was marked as uncompleted.`);
                        }
                    });
                }
                save(dataList);
            },
            description: "Mark task as uncompleted.\n"
        };

        //delete a task
        this.delete = {
            function: async function(...tasksIDToBeDeleted: string[]): Promise<void>{
                const dataList: ITodosList = await load();
                for(const taskIDToDelete of tasksIDToBeDeleted){
                    dataList.forEach(todo => {
                        if(todo.id === taskIDToDelete){
                            const indexOfTodoToRemove = dataList.indexOf(todo);
                            if (indexOfTodoToRemove > -1) {
                                dataList.splice(indexOfTodoToRemove, 1);
                                log.red(`Todo '${todo.title}' was deleted.`);
                            }
                        }
                    });
                }
                save(dataList);
            },
            description: "Delete a given task(s).\n"
        };

        this.removeCompleted = {
        function: async function(): Promise<void>{
            const dataList: ITodosList = await load();
            let listWithoutCompleted: ITodosList= [];
            listWithoutCompleted = dataList.filter(todo=> todo.isCompleted === false);
            if(dataList.length - listWithoutCompleted.length === 0) {
                log.yellow("There were no completed todos.")
            } else {
                log.red(`All completed todos was deleted.`);
                save(listWithoutCompleted);
            }
            
            // log.obj(listWithoutCompleted,`list after remove all completed`);
        },
        description: "Remove all comleted todos.\n"
    };
    }

    //using public static method to get the instance only once 
    public static getInstance(): CommandsObject {
        if (!CommandsObject.instance) {
            CommandsObject.instance = new CommandsObject();
        }

        return CommandsObject.instance;
    }
}