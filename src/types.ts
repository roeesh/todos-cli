export interface ITodo {
    id: string;
    title: string;
    isCompleted: boolean;
}

export type ITodosList = ITodo[];