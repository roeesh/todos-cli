import log from '@ajar/marker';
import { argv } from 'process';
import CommandsObject, {ICommandsObject} from './controller/commands-object.js';
import { showFunctionsOptions } from './view/cli-outputs.js'
import { generateId } from './utils/utils-string.js';
import { ITodo, ITodosList } from './types.js';
import { save, load, isFileExists } from './database/data-access.js';
import path from 'path/posix';


// const ALL = "all";
// const COMPLETED = "completed";
// const OPEN = "open";

//singelton command-object

// interface ICommand{
//     function: Function;
//     description: string;
// }

// interface ICommandsObject {
//     [key:string]: ICommand;
// }

// interface IDescriptionObject {
//     [key:string]: string;
// }


//object with the available functions as keys
// const commandsObj : ICommandsObject = {
//     //create new task
//     create: {
//         function: async function(...title: string[]): Promise<void> {
//             const dataList: ITodosList = await load();
//             // console.log(dataList);
//             let newTask: ITodo = {
//                 id: generateId(),
//                 title: title.join(' '),
//                 isCompleted: false
//             }
//             dataList.push(newTask);
//             log.yellow(`Todo '${newTask.title}' was added to your todos list.\n`)
//             save(dataList);
//         },
//         description: "Create new task(s) by enter the name(s).\n"
//     },

//     //read tasks by the filter-word  
//     read: {
//         function: async function(filterWord: string): Promise<void> {
//             const dataList: ITodosList = await load();
//             let listToShow = [];
//             if(dataList.length === 0) {
//                 log.cyan("Your Todos List is empty.\n")
//             }else{
//                 log.cyan("Your Todos List:\n");
//                 switch(filterWord) {
//                     case ALL:
//                         console.table(dataList);
//                         console.log('\n');
//                         return;
                    
//                     case COMPLETED:
//                         listToShow = dataList.filter(todo=> todo.isCompleted === true);
//                         console.table(listToShow);
//                         return;
                    
//                     case OPEN:
//                         listToShow = dataList.filter(todo=> todo.isCompleted === false);
//                         console.table(listToShow);
//                         return;
    
//                     default:
//                         console.table(dataList);
//                 }
                
//                 // for(const todo of dataList){
//                 //     log.blue(todo.title);
//                 //     console.table(todo);
                
//             }
            
//         },
//         description: "  Read tasks: write 'all' / 'completed' / 'open' after the read command\n         in order to display the wanted todos.\n"
//     },

//     //update a complete flag for each task
//     update: {
//         function: async function(...tasksIDToBeCompleted: string[]): Promise<void>{
//             const dataList: ITodosList = await load();
//             for(const taskIDToComplete of tasksIDToBeCompleted){
//                 dataList.forEach(todo => {
//                     if(todo.id === taskIDToComplete){
//                         todo.isCompleted = true;
//                         log.yellow(`Todo '${todo.title}' was updated.`);
//                     }
//                 });
//             }
//             save(dataList);
//         },
//         description: "Update a complete flag for each task.\n"
//     },

//     //delete a task
//     delete: {
//         function: async function(...tasksIDToBeDeleted: string[]): Promise<void>{
//             const dataList: ITodosList = await load();
//             for(const taskIDToDelete of tasksIDToBeDeleted){
//                 dataList.forEach(todo => {
//                     if(todo.id === taskIDToDelete){
//                         const indexOfTodoToRemove = dataList.indexOf(todo);
//                         if (indexOfTodoToRemove > -1) {
//                             dataList.splice(indexOfTodoToRemove, 1);
//                             log.red(`Todo '${todo.title}' was deleted.`);
//                         }
//                     }
//                 });
//             }
//             save(dataList);
//         },
//         description: "Delete a given task(s).\n"
//     },

//     removeCompleted: {
//         function: async function(): Promise<void>{
//             const dataList: ITodosList = await load();
//             let listWithoutCompleted: ITodosList= [];
//             listWithoutCompleted = dataList.filter(todo=> todo.isCompleted === false);
//             if(dataList.length - listWithoutCompleted.length === 0) {
//                 log.yellow("There were no completed todos.")
//             } else {
//                 log.red(`All completed todos was deleted.`);
//                 save(listWithoutCompleted);
//             }
            
//             // log.obj(listWithoutCompleted,`list after remove all completed`);
//         },
//         description: "Remove all comleted todos.\n"
//     }
// }

//show which functions are available

// function showFunctionsOptions(commandsObj): void{
//     log.blue(`\nAvailable commands are:\n\n${
//         Object.keys(commandsObj)
//         .map(command => `${command} - ${commandsObj[command].description} \n`)
//         .join('')
//     }\n`);
// }

// const descriptionObj: IDescriptionObject = {
//     create: "Create new task(s) by enter the name(s).\n",
//     read: "  Read tasks: write 'all' / 'completed' / 'open' after the read command\n         in order to display the wanted todos.\n",
//     update: "Update a complete flag for each task.\n",
//     delete: "Delete a given task(s).\n",
//     removeCompleted: "Remove all comleted todos.\n"
// }

// interface IDescriptionObject {
//     create :string;
//     read :string;
//     update :string;
//     delete :string;
//     removeCompleted: string;
// }


const commandsObj: ICommandsObject = CommandsObject.getInstance();


(async function init(): Promise<void>{

    //if file doesn't exist - save() will create it with empty list(defalt value)
    if(await isFileExists() === false){
        save();
    }

    if(argv[2]) {
        const [command, ...args] = [argv[2], ...argv.slice(3)];

        if(command === "help"){
            showFunctionsOptions(commandsObj);
        }

        //if the command is a valid one- execute the function using the commandObject
        if(command in commandsObj){
            commandsObj[command].function(...args);
        } else if(command !== "help") {
            log.red(`No such command: '${command}'.\nPlease type 'help' to see the commands.`)
        }
    } else{
        log.cyan("Welcome to todos-cli\nTo see the commands enter 'help'");
        console.log('\n');
    }
})()


// init();


