import { ITodo, ITodosList } from '../types.js'
import log from '@ajar/marker';
import fs from 'fs/promises';
const currentPath = ".";

//save the JSON data file - if doesn't exist create a new one
export async function save(dataList: ITodosList = []): Promise<void> {
    await fs.writeFile('./todos.json',JSON.stringify(dataList,null,2));
    log.green('File written successfully!');
}

//load the JSON data file
export async function load(): Promise<ITodosList> {
    // if(isFileExists() === true){
        let filesNames: string[] = await fs.readdir(currentPath);
        if(filesNames.includes("todos.json") === true){
            let fileContent: string = await fs.readFile(`${currentPath}/todos.json`,'utf-8');
            return JSON.parse(fileContent.length ? fileContent: '[]')
        }
    // }
     else {
        log.red("file 'todos.json' doesn't exist");
        return [];
    }
}

//checks if the DB file exists
export async function isFileExists(): Promise<boolean> {
    let filesNames: string[] = await fs.readdir(currentPath);
    // log.cyan(filesNames)
    if(filesNames.includes("todos.json") === true){
        return true;
    }else{
        return false;
    }
}