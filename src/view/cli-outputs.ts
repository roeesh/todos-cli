import log from '@ajar/marker';
import CommandsObject, {ICommandsObject} from '../controller/commands-object.js'

//show all the available commands
export function showFunctionsOptions(commandsObj: ICommandsObject): void{
    log.blue(`\nAvailable commands are:\n\n${
        Object.keys(commandsObj)
        .map(command => `${command} - ${commandsObj[command].description} \n`)
        .join('')
    }\n`);
}

//print the todos list
